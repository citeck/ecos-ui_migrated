import { PROXY_URI } from '../constants/alfresco';
import { SourcesId } from '../constants';
import Records from '../components/Records';
import { t } from '../helpers/util';
import { CommonApi } from './common';
import { isNewJournalsPageEnable } from './export/journalsApi';

export class UserApi extends CommonApi {
  getPhotoSize = userNodeRef => {
    const url = `${PROXY_URI}citeck/node?nodeRef=${userNodeRef}&props=ecos:photo`;
    return this.getJson(url).then(data => {
      if (!data.props || !data.props['ecos:photo']) {
        return 0;
      }

      return data.props['ecos:photo'].size;
    });
  };

  getUserData = () => {
    return Records.query(
      {
        sourceId: SourcesId.PEOPLE
      },
      {
        userName: 'userName',
        isAvailable: 'isAvailable?bool',
        isDeputyAvailable: 'deputy:available?bool',
        isMutable: 'isMutable?bool',
        firstName: 'cm:firstName',
        lastName: 'cm:lastName',
        middleName: 'cm:middleName',
        isAdmin: 'isAdmin?bool',
        fullName: 'fullName',
        uid: 'cm:name',
        isBpmAdmin: '.att(n:"authorities"){has(n:"GROUP_BPM_APP_ADMIN")}',
        nodeRef: 'nodeRef?str',
        modified: '_modified?str',
        avatar: 'avatar.url'
      }
    ).then(resp => {
      if (resp.records.length < 1) {
        return {
          success: false
        };
      }

      return {
        success: true,
        payload: {
          ...resp.records[0]
        }
      };
    });
  };

  isUserAdmin = () => {
    return Records.queryOne({ sourceId: SourcesId.PEOPLE }, 'isAdmin?bool');
  };

  getUserDataByRef(ref) {
    return Records.get(ref).load({
      userName: 'userName',
      firstName: 'cm:firstName',
      lastName: 'cm:lastName',
      middleName: 'cm:middleName',
      isAdmin: 'isAdmin?bool',
      nodeRef: 'nodeRef?str',
      modified: '_modified?str'
    });
  }

  changePassword({ record, data: { oldPass, pass, passVerify } }) {
    const user = Records.get(record);

    user.att('ecos:pass', pass);
    user.att('ecos:passVerify', passVerify);
    user.att('ecos:oldPass', oldPass);

    return user
      .save()
      .then(response => ({ response, success: true }))
      .catch(response => {
        let message = response.message || (response.errors && response.errors.join('; ')) || '';

        if (message.indexOf('BadCredentials') !== -1) {
          message = t('password-editor.error.invalid-password');
        } else {
          message = t('password-editor.error.server-error');
        }

        return { success: false, message };
      });
  }

  changePhoto({ record, data }) {
    const user = Records.get(record);

    user.att('ecos:photo?str', { ...data });

    return user
      .save()
      .then(response => ({ response, success: true }))
      .catch(response => {
        const message = response.message || (response.errors && response.errors.join('; ')) || '';

        return { success: false, message };
      });
  }

  checkNewUIAvailableStatus() {
    return isNewJournalsPageEnable();
  }
}
