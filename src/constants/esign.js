export const ErrorTypes = {
  DEFAULT: 'default',
  NO_CADESPLUGIN: 'no-cadesplugin',
  SIGNED: 'signed',
  NO_CERTIFICATES: 'no-certificates'
};

export const Labels = {
  ERROR: 'esign.error',
  ADD_PLUGIN: 'esign.add-plugin.title',
  ADD_PLUGIN_MESSAGE: 'esign.message.add-plugin',
  NODE_NOT_FOUND_MESSAGE: 'esign.message.node-not-found',
  NODE_REF_NOT_FOUND: 'esign.no-document-ref',
  NO_CERTIFICATES_MESSAGE: 'esign.message.no-certificates',
  NO_BASE64_DOC_MESSAGE: 'esign.message.no-document-base64',
  NO_CERTIFICATE_THUMBPRINT_MESSAGE: 'esign.message.no-certificate-data',
  SIGN_FAILED_VERIFICATION_MESSAGE: 'esign.message.sign-failed-verification',
  SIGN_FAILED_MESSAGE: 'esign.message.sign-failed',
  PLUGIN_IS_FETCHING_MESSAGE: 'esign.message.plugin-is-fetching',

  MODAL_TITLE: 'esign.modal.sign.title',
  MODAL_ALREADY_SIGNED_TITLE: 'esign.message.document-has-been-signed',

  GO_TO_PLUGIN_PAGE_BTN: 'esign.btn.go-to-plugin-page',
  OK_BTN: 'esign.btn.ok',
  SIGN_BTN: 'esign.btn.sign',
  CANCEL_BTN: 'esign.btn.cancel',

  MODAL_CERTIFICATE_PROPERTIES: 'esign.modal.certificate-properties',
  MODAL_SUBJECT: 'esign.modal.subject',
  MODAL_ISSUER: 'esign.modal.issuer',
  MODAL_DATE_FROM: 'esign.modal.date-from',
  MODAL_DATE_TO: 'esign.modal.date-to',
  MODAL_PROVIDER: 'esign.modal.provider',

  NO_DATA: 'esign.no-data'
};

export const PLUGIN_URL = 'https://www.cryptopro.ru/products/cades/plugin';
