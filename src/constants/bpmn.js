export const Labels = {
  ADD_CATEGORY: 'bpmn-designer.add-category',
  TOTAL: 'bpmn-designer.total',
  CreateVariants: {
    CREATE_MODEL: 'bpmn-designer.create-model',
    IMPORT_MODEL: 'bpmn-designer.import-model'
  },
  Views: {
    CARDS: 'bpmn-designer.view-mode.cards',
    LIST: 'bpmn-designer.view-mode.list'
  }
};

export const ViewTypes = {
  CARDS: 'cards',
  LIST: 'list',
  TABLE: 'table'
};

export const SORT_FILTER_LAST_MODIFIED = 0;
export const SORT_FILTER_OLD = 1;
export const SORT_FILTER_AZ = 2;
export const SORT_FILTER_ZA = 3;

export const ROOT_CATEGORY_NODE_REF = 'workspace://SpacesStore/ecos-bpm-category-root';

export const EDITOR_PAGE_CONTEXT = '/share/page/bpmn-editor/';

export const LOCAL_STORAGE_KEY_PAGE_POSITION = 'BpmnPagePosition';
export const LOCAL_STORAGE_KEY_REFERER_PAGE_PATHNAME = 'BpmnRefererPagePathName';

export const PREFIX_FORM_ELM = '@bpmn-type-';
export const TYPE_BPMN_PROCESS = 'bpmn:Process';
