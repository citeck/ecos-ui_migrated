import React from 'react';
import { t } from '../../helpers/export/util';

const UnreadableLabel = () => {
  return <div className="ecos-form__value_unreadable">{t('ecos-form.value-unreadable')}</div>;
};

export default UnreadableLabel;
