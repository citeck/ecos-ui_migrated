import TextArea from './TextArea';
import TextAreaEditor from 'formiojs/components/textarea/TextArea.form';

TextArea.editForm = TextAreaEditor;

export default TextArea;
