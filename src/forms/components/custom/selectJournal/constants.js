export const SortOrderOptions = {
  ASC: {
    label: 'ASC',
    value: 'asc'
  },
  DESC: {
    label: 'DESC',
    value: 'desc'
  }
};

export const TableTypes = {
  JOURNAL: 'journal',
  CUSTOM: 'custom'
};

export const DisplayModes = {
  DEFAULT: 'default',
  TABLE: 'table'
};
